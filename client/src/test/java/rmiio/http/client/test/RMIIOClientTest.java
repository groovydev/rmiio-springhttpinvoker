/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.client.test;

import org.testng.annotations.Test;
import org.testng.spring.test.AbstractDependencyInjectionSpringContextTests;

import rmiio.http.client.FileTransferAction;


/**
 * Client caller
 *  
 */
public class RMIIOClientTest extends
		AbstractDependencyInjectionSpringContextTests {

	/**
	 * Service FileTransferService
	 */
	private static FileTransferAction fileTransferAction;

	@Override
	protected void onSetUp() throws Exception {
		super.onSetUp();
		fileTransferAction = (FileTransferAction) applicationContext
				.getBean("fileTransferAction");
	}

	
	/**
	 * test UploadFileWithRMIIO
	 * 
	 * @throws Exception
	 */
	@Test
	public void testUploadFileWithRMIIO() throws Exception {
		fileTransferAction.uploadFileWithRMIIO("D:\\tmpRMIIO\\to-be-uploaded.war",
				"D:\\tmpRMIIO\\server");
	}

	/**
	 * test DownloadFileWithRMIIO
	 * 
	 * @throws Exception
	 */
	@Test
	public void testDownloadFileWithRMIIO() throws Exception {
		fileTransferAction.downloadFileWithRMIIO("D:\\tmpRMIIO\\server\\to-be-downloaded.war",
				"D:\\tmpRMIIO");
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "service.xml" };
	}
}
