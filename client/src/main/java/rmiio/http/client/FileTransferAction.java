/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.LogFactoryImpl;

import rmiio.http.api.service.FileTransferService;
import rmiio.http.api.service.RMIIOHTTPException;
import rmiio.http.api.service.RemoteInputStreamProxy;

import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamClient;
import com.healthmarketscience.rmiio.RemoteOutputStream;
import com.healthmarketscience.rmiio.RemoteOutputStreamClient;


/**
 * FileTransferAction
 * 
 */
public class FileTransferAction {
    /**
     * Service FileTransferService
     */
    public FileTransferService fileTransferService;
   
    /**
     * Temporary working directory
     */
    private final String TEMP_FOLDER_FILE_TO_SEND = "C:\\tmpFileIn";

    /**
     * Logger
     */
    private Log logger = LogFactoryImpl.getLog(FileTransferAction.class);

   

    /**
     * Upload file with RMIIO
     * 
     * @param sourcePath
     *            source file to upload
     * @param targetPath
     *            folder path on server to upload a file
     * @throws RMIIOHTTPException
     */
    public void uploadFileWithRMIIO(String sourcePath, String targetPath)
            throws RMIIOHTTPException {
        BufferedInputStream bsi = null;
        FileInputStream fis = null;
        OutputStream os = null;
        try {
            File fileToUpload = new File(sourcePath);
            // Check if file exist
            if (fileToUpload.exists() && fileToUpload.isFile()) {
                // Call the uploadFileRMIIO and get the RemoteOutputStream
                RemoteOutputStream ros = fileTransferService.uploadFile(fileToUpload.getName(), fileToUpload
                        .length(), targetPath);               
                
                os = RemoteOutputStreamClient.wrap(ros);
                fis = new FileInputStream(sourcePath);
                bsi = new BufferedInputStream(fis);
                byte[] tempBuffer = new byte[1024];

                int nbRead = 0;
                while ((nbRead = bsi.read(tempBuffer)) != -1) {
                    os.write(tempBuffer, 0, nbRead);
                }
            } else if (!fileToUpload.exists()) {
                logger.info("file " + sourcePath + " don't exist");
                throw new RMIIOHTTPException("file " + sourcePath + " don't exist");
            } else {
                logger.info(sourcePath + " is not a file");
                throw new RMIIOHTTPException(sourcePath + " is not a file");
            }
        } catch (IOException e) {
            logger.info(e.getMessage());
            throw new RMIIOHTTPException(e);
        } finally {
            IOUtils.closeQuietly(os);
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                }
            }
        }
    }

   
    /**
     * 
     * @param sourcePath
     *            source file to download
     * @param targetPath
     *            folder path on local used to download a file
     * @throws RMIIOHTTPException
     */
    public void downloadFileWithRMIIO(String sourcePath, String targetPath)
            throws RMIIOHTTPException {
        InputStream ris = null;
        logger.info("downloadFileWithRMIIO is called  -- >" + sourcePath);
        BufferedOutputStream output = null;
        FileOutputStream io = null;

        try {
            File sourceFile = new File(sourcePath);
            // Call download file with service and return a RemoteInputStream
            RemoteInputStream remoteInputStrem = fileTransferService.downloadFile(sourcePath);
            
            // Wrap the remoteInputStream to get a input stream which we will be
            // used it normaly
            ris = RemoteInputStreamClient.wrap(remoteInputStrem);

            File file = createFile(targetPath, sourceFile.getName());
            io = new FileOutputStream(file);
            if (file != null) {
                output = new BufferedOutputStream(io);
                byte[] tempBuffer = new byte[1024];

                int nbRead = 0;
                // check if the remoteInputStrem is not closed
                while (!((RemoteInputStreamProxy) remoteInputStrem).isClosed() && (nbRead = ris.read(tempBuffer)) != -1) {
                    output.write(tempBuffer, 0, nbRead);
                }
            }
        } catch (FileNotFoundException e) {
            logger.info(e.getMessage());
            throw new RMIIOHTTPException(e);
        } catch (IOException e) {
            logger.info(e.getMessage());
            throw new RMIIOHTTPException(e);
        } finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(io);
            if (ris != null) {
                try {
                    ris.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Create temporary file
     * 
     * @param nameFile
     * @return
     */
    public File createTempFile(String nameFile) {
        File tmpFile = new File(TEMP_FOLDER_FILE_TO_SEND + File.separator + nameFile);
        try {
            tmpFile.createNewFile();
        } catch (IOException e) {
            tmpFile = null;
            e.printStackTrace();
        }
        return tmpFile;
    }

    /**
     * Create a file
     * 
     * @param folderParent
     * @param nameFile
     * @return
     */
    public File createFile(String folderParent, String nameFile) {
        File tmpFile = new File(folderParent + File.separator + nameFile);
        try {
            tmpFile.createNewFile();
        } catch (IOException e) {
            tmpFile = null;
            e.printStackTrace();
        }
        return tmpFile;
    }

    /**
     * Setter fileTransferService
     * 
     * @param fileTransferService
     */
    public void setFileTransferService(FileTransferService fileTransferService) {
        this.fileTransferService = fileTransferService;
    }

}
