/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

/**
 * Constant
 * 
 */
public class APIConstants {
	/**
	 * Class name of exporter
	 */
	public static final String EXPORTER_CLASS_NAME = "fr.sqli.server.rmiio.service.impl.RemoteStreamExporterHttp";
}
