/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

/**
 *  
 */
public class RMIIOHTTPException extends Exception {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * See : {@link Exception#Exception()}
	 */
	public RMIIOHTTPException() {
	}

	/**
	 * See : {@link Exception#Exception(String)}
	 */
	public RMIIOHTTPException(String message) {
		super(message);
	}

	/**
	 * See : {@link Exception#Exception(Throwable)}
	 */
	public RMIIOHTTPException(Throwable cause) {
		super(cause);
	}

	/**
	 * See : {@link Exception#Exception(String, Throwable)}
	 */
	public RMIIOHTTPException(String message, Throwable cause) {
		super(message, cause);
	}
}
