/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;

import rmiio.http.api.service.utils.SpringUtil;

import com.healthmarketscience.rmiio.RemoteInputStream;


/**
 * Proxy's input stream sent to client
 * 
 */
public class RemoteInputStreamProxy implements RemoteInputStream, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Key Stream
	 */
	private final String _key;
	/**
	 * Key Stream
	 */
	private String keyMonitoring;
	/**
	 * true if the stream is closed
	 */
	private boolean closed = false;

	/**
	 * Constructor
	 * 
	 * @param pKey
	 * @param pRegistry
	 */
	public RemoteInputStreamProxy(String pKey) {
		this._key = pKey;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] readPacket(int packetId) throws RemoteException, IOException {
		return getBeanRegisterStreamsManager().readPacket(_key, packetId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException, RemoteException {
		return getBeanRegisterStreamsManager().available(_key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close(boolean readSuccess) throws IOException, RemoteException {
		getBeanRegisterStreamsManager().close(_key, readSuccess);
		closed = true;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip(long n, int skipId) throws IOException, RemoteException {
		return getBeanRegisterStreamsManager().skip(_key, n, skipId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean usingGZIPCompression() throws IOException, RemoteException {
		return getBeanRegisterStreamsManager().usingGZIPCompression(_key);
	}

	/**
	 * Return a the BeanRegisterStreamsManager
	 * 
	 * @return
	 */
	private RegistryInputStreamsManager getBeanRegisterStreamsManager() {
		return (RegistryInputStreamsManager) SpringUtil
				.getApplicationContext().getBean(
						RegistryInputStreamsManager.BEAN_NAME);
	}

	/**
	 * Getter keyMonitoring
	 * 
	 * @return
	 */
	public String getKeyMonitoring() {
		return keyMonitoring;
	}

	/**
	 * Getter keyMonitoring
	 * 
	 * @param keyMonitoring
	 */
	public void setKeyMonitoring(String keyMonitoring) {
		this.keyMonitoring = keyMonitoring;
	}

	/**
	 * Return true if the stream is closed
	 * 
	 * @return
	 */
	public boolean isClosed() {
		return closed;
	}
}
