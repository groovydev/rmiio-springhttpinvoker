/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

/**
 * Interface MonitoringManagerService to monitor percent's transfer of streams
 * 
 */
public interface MonitoringManagerService {
	/**
	 * Update percent's transfer file
	 * 
	 * @param keyMonitor
	 *            key's monitor
	 * @param percentTransfer
	 *            new percent value of transfer
	 */
	public void setPercentTransferFile(String keyMonitor, double percentTransfer);

	/**
	 * Return percent value of transfer file
	 * 
	 * @param keyMonitor
	 * @return
	 */
	public double getPercentTransferFile(String keyMonitor);

	/**
	 * If the transfer finish we remove the percent values
	 * 
	 * @param keyMonitor
	 */
	public void removePercentTransferFile(String keyMonitor);
}
