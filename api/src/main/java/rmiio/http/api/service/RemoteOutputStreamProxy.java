/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;

import rmiio.http.api.service.utils.SpringUtil;

import com.healthmarketscience.rmiio.RemoteOutputStream;


/**
 * Proxy's output stream sent to the client
 * 
 */
public class RemoteOutputStreamProxy implements RemoteOutputStream,
		Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Key Stream
	 */
	private final String _key;
	/**
	 * Key Stream
	 */
	private String keyMonitoring;
	/**
	 * true if the stream is closed
	 */
	private boolean closed = false;

	/**
	 * Constructor
	 * 
	 * @param key
	 */
	public RemoteOutputStreamProxy(String key) {
		this._key = key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close(boolean writeSuccess) throws IOException, RemoteException {
		getBeanRegisterOutputStreamsManager().close(_key, writeSuccess);
		closed = true;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException, RemoteException {
		getBeanRegisterOutputStreamsManager().flush(_key);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean usingGZIPCompression() throws IOException, RemoteException {
		return getBeanRegisterOutputStreamsManager().usingGZIPCompression(_key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writePacket(byte[] packet, int packetId) throws IOException,
			RemoteException {
		getBeanRegisterOutputStreamsManager().writePacket(_key, packet,
				packetId);

	}

	/**
	 * Return a the Bean RegisterOutputStreamsManager
	 * 
	 * @return
	 */
	private RegistryOutputStreamsManager getBeanRegisterOutputStreamsManager() {
		return (RegistryOutputStreamsManager) SpringUtil
				.getApplicationContext().getBean(
						RegistryOutputStreamsManager.BEAN_NAME);
	}

	/**
	 * Getter keyMonitoring
	 * 
	 * @return
	 */
	public String getKeyMonitoring() {
		return keyMonitoring;
	}

	/**
	 * Getter keyMonitoring
	 * 
	 * @param keyMonitoring
	 */
	public void setKeyMonitoring(String keyMonitoring) {
		this.keyMonitoring = keyMonitoring;
	}

	/**
	 * Return true if the stream is closed
	 * 
	 * @return
	 */
	public boolean isClosed() {
		return closed;
	}

}
