/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package rmiio.http.api.service;

import java.io.IOException;

import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteOutputStream;


/**
 * 
 * Service used to transfer files
 *  
 */
public interface FileTransferService {

	/**
	 * Upload file using RMIIO (Send file from client to server).
	 * 
	 * @param fileName
	 * @param lengthOfFile
	 *            used on monitoring
	 * @return
	 * @throws IOException
	 */
	RemoteOutputStream uploadFile(String fileName, double lengthOfFile,
			String targetFolder) throws RMIIOHTTPException;

	/**
	 * Download file using RMIIO (Send file from server to client).
	 * 
	 * @param fileName
	 * @return
	 */
	RemoteInputStream downloadFile(String fileName)
			throws RMIIOHTTPException;

}
