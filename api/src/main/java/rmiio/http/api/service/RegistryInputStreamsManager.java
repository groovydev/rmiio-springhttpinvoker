/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

import java.io.IOException;
import java.rmi.RemoteException;

import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamServer;

/**
 * Store the <li>RemoteInputStream</li> and override methods of
 * RemoteInputStream {@link RemoteInputStream} but we add a parameter Key used
 * to access to the stream
 * 
 */
public interface RegistryInputStreamsManager {
	/**
	 * Name's bean registryInputStreamsManager.
	 */
	public static final String BEAN_NAME = "registryInputStreamsManager";

	/**
	 * Store the stream which will be accessible from client
	 * 
	 * @param stream
	 * @return RemoteInputStream
	 */
	public RemoteInputStream register(RemoteInputStreamServer stream);

	/**
	 * Remove the stream exported.
	 * 
	 * @param stream
	 */
	public void unRegister(RemoteInputStream StreamType);

	/**
	 * {@link RemoteInputStream#readPacket(int)}
	 * 
	 * @param key
	 * @param packetId
	 * @return
	 * @throws RemoteException
	 * @throws IOException
	 */
	public byte[] readPacket(String key, int packetId) throws RemoteException,
			IOException;

	/**
	 * {@link RemoteInputStream#close(boolean)}
	 * 
	 * @param key
	 * @param readSucces
	 * @throws RemoteException
	 * @throws IOException
	 */
	public void close(String key, boolean readSucces) throws RemoteException,
			IOException;

	/**
	 * {@link RemoteInputStream#available()}
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 * @throws RemoteException
	 */
	public int available(String key) throws RemoteException, IOException;

	/**
	 * {@link RemoteInputStream#skip(long, int)}
	 * 
	 * @param key
	 * @param n
	 * @param skipId
	 * @return
	 * @throws IOException
	 * @throws RemoteException
	 */
	public long skip(String key, long n, int skipId) throws RemoteException,
			IOException;

	/**
	 * {@link RemoteInputStream#usingGZIPCompression()}
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 * @throws RemoteException
	 */
	public boolean usingGZIPCompression(String key) throws RemoteException,
			IOException;
}
