/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.api.service;

import java.io.IOException;
import java.rmi.RemoteException;

import com.healthmarketscience.rmiio.RemoteOutputStream;
import com.healthmarketscience.rmiio.RemoteOutputStreamServer;

/**
 * Store the <li>RemoteOutputStream</li> and override methods of
 * RemoteOutputStream {@link RemoteOutputStream} but we add a parameter Key used
 * to access to the stream
 * 
 */
public interface RegistryOutputStreamsManager {

	/**
	 * Name's bean registryOutputStreamsManager
	 */
	public static final String BEAN_NAME = "registryOutputStreamsManager";

	/**
	 * Store the stream which will be accessible from client
	 * 
	 * @param stream
	 * @return
	 */
	public RemoteOutputStream register(RemoteOutputStreamServer stream);

	/**
	 * Remove the stream exported.
	 * 
	 * @param stream
	 */
	public void unRegister(RemoteOutputStream stream);

	/**
	 * {@link RemoteOutputStream#close(boolean)}
	 * 
	 * @param key
	 * @param writeSuccess
	 * @throws IOException
	 * @throws RemoteException
	 */
	public void close(String key, boolean writeSuccess) throws IOException,
			RemoteException;

	/**
	 * {@link RemoteOutputStream#flush()}
	 * 
	 * @param key
	 * @throws IOException
	 * @throws RemoteException
	 */
	public void flush(String key) throws IOException, RemoteException;

	/**
	 * {@link RemoteOutputStream#usingGZIPCompression()}
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 * @throws RemoteException
	 */
	public boolean usingGZIPCompression(String key) throws IOException,
			RemoteException;

	/**
	 * {@link RemoteOutputStream#writePacket(byte[], int)}
	 * 
	 * @param key
	 * @param packet
	 * @param packetId
	 * @throws IOException
	 * @throws RemoteException
	 */
	public void writePacket(String key, byte[] packet, int packetId)
			throws IOException, RemoteException;
}
