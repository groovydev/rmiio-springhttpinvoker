/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import rmiio.http.api.service.RMIIOHTTPException;


/**
 * Contains methods utils to manage file
 *  
 */
public class FileManagerUtils {
    /**
     * Create Temporary file
     * 
     * @param configurationProperties
     * @return
     */
    public static File getTempFile(Properties configurationProperties) {
        return getTempFileByName("", configurationProperties);
    }

    /**
     * Create Temporary file by name (System.currentTimeMillis() +"_"+ fileName)
     * 
     * @param fileName
     * @param configurationProperties
     * @return
     */
    public static File getTempFileByName(String fileName, Properties configurationProperties) {
        File tempWorkingFolder = new File(configurationProperties.getProperty("temp.directory")
                + File.separator + System.currentTimeMillis());
        if (!tempWorkingFolder.exists()) {
            tempWorkingFolder.mkdir();
        }
        File tempWorkingFile = new File(tempWorkingFolder.getAbsolutePath() + File.separator
                + System.currentTimeMillis() + "_" + fileName);
        if (!tempWorkingFile.exists()) {
            try {
                tempWorkingFile.createNewFile();
            } catch (IOException e) {
            }
        }
        return tempWorkingFile;
    }

    /**
     * Create a file
     * 
     * @param folderParent
     *            Patent folder of file, must be exist to create file
     * @param nameFile
     *            Name's file
     * @return
     */
    public static File createFile(String folderParent, String nameFile) throws RMIIOHTTPException {
        File tmpFile = new File(folderParent + File.separator + nameFile);
        try {
            tmpFile.createNewFile();
        } catch (Throwable e) {
            throw new RMIIOHTTPException(e);
        }
        return tmpFile;
    }
}
