/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import java.rmi.RemoteException;

import rmiio.http.api.service.RegistryInputStreamsManager;
import rmiio.http.api.service.RegistryOutputStreamsManager;
import rmiio.http.api.service.utils.SpringUtil;

import com.healthmarketscience.rmiio.RemoteInputStreamServer;
import com.healthmarketscience.rmiio.RemoteOutputStreamServer;
import com.healthmarketscience.rmiio.RemoteStreamServer;
import com.healthmarketscience.rmiio.exporter.RemoteStreamExporter;


/**
 * RemoteStreamExporter implementation to send streams over HTTP
 * 
 */
public class RemoteStreamExporterHttp extends RemoteStreamExporter {

    /**
     * {@inheritDoc}
     */
    @Override
    public Object exportImpl(RemoteStreamServer<?, ?> server) throws RemoteException {
        // we register the server on the registry to be accessil from a client
        if (server instanceof RemoteInputStreamServer) {
            // if the server is type of inputstream we use the
            // BeanRegisterInputStreamsManager
            return getBeanRegisterInputStreamsManager().register((RemoteInputStreamServer) server);
        } else if (server instanceof RemoteOutputStreamServer) {
            // we use BeanRegisterOutputStreamsManager
            return getBeanRegisterOutputStreamsManager().register((RemoteOutputStreamServer) server);
        } else {
            throw new IllegalArgumentException(
                    "The arrgument must be instance of RemoteOutputStreamServer or RemoteInputStreamServer ");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unexportImpl(RemoteStreamServer<?, ?> server) throws Exception {
        // we remote the server from the registry
        if (server instanceof RemoteInputStreamServer) {
            // if the server is type of inputstream we use the
            // BeanRegisterInputStreamsManager
            getBeanRegisterInputStreamsManager().unRegister((RemoteInputStreamServer) server);
        } else if (server instanceof RemoteOutputStreamServer) {
            // we use BeanRegisterOutputStreamsManager
            getBeanRegisterOutputStreamsManager().unRegister((RemoteOutputStreamServer) server);

        } else {
            throw new IllegalArgumentException(
                    "The arrgument must be instance of RemoteOutputStreamServer or RemoteInputStreamServer ");
        }
    }

    /**
     * Return a the Bean RegisterInputStreamsManager
     * 
     * @return
     */
    private RegistryInputStreamsManager getBeanRegisterInputStreamsManager() {
        return (RegistryInputStreamsManager) SpringUtil.getApplicationContext().getBean(
                RegistryInputStreamsManager.BEAN_NAME);
    }

    /**
     * Return a the Bean RegisterStreamsManager
     * 
     * @return
     */
    private RegistryOutputStreamsManager getBeanRegisterOutputStreamsManager() {
        return (RegistryOutputStreamsManager) SpringUtil.getApplicationContext().getBean(
                RegistryOutputStreamsManager.BEAN_NAME);
    }
}
