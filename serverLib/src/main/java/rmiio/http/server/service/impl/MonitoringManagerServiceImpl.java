/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import java.util.HashMap;
import java.util.Map;

import rmiio.http.api.service.MonitoringManagerService;


/**
 * Implementation's MonitoringManagerService
 * 
 */
public class MonitoringManagerServiceImpl implements MonitoringManagerService {
    /**
     * Map contains the percent value of transfer file by key's monitor
     */
    private static Map<String, Double> mapPurcenteDownload = new HashMap<String, Double>();

    /**
     * {@inheritDoc}
     */
    @Override
    public double getPercentTransferFile(String keyStream) {
        synchronized (mapPurcenteDownload) {
            Double result = mapPurcenteDownload.get(keyStream);
            if (result != null) {
                return result;
            }
            return 0;

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPercentTransferFile(String keyStream, double purcenteDownload) {
        synchronized (mapPurcenteDownload) {
            mapPurcenteDownload.put(keyStream, purcenteDownload);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removePercentTransferFile(String keyStream) {
        synchronized (mapPurcenteDownload) {
            Double result = mapPurcenteDownload.get(keyStream);
            if (result != null) {
                mapPurcenteDownload.remove(keyStream);
            }
        }
    }
}
