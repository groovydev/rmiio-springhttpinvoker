/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.LogFactoryImpl;

import rmiio.http.api.service.FileTransferService;
import rmiio.http.api.service.RMIIOHTTPException;
import rmiio.http.api.service.RemoteInputStreamProxy;
import rmiio.http.api.service.RemoteOutputStreamProxy;

import com.healthmarketscience.rmiio.GZIPRemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamServer;
import com.healthmarketscience.rmiio.RemoteOutputStream;
import com.healthmarketscience.rmiio.RemoteOutputStreamServer;
import com.healthmarketscience.rmiio.SimpleRemoteOutputStream;
import com.healthmarketscience.rmiio.exporter.RemoteStreamExporter;


/**
 * Implementation's FileTransferService
 *  
 */
public class FileTransferServiceImpl implements FileTransferService {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Bean configurationProperties
     */
    private Properties configurationProperties;

    /**
     * Bean remoteStreamExporterHTTP
     */
    private RemoteStreamExporter remoteStreamExporterHTTP;

    /**
     * Logger
     */
    private final Log logger = LogFactoryImpl.getLog(FileTransferServiceImpl.class);

    
    /**
     * {@inheritDoc}
     */
    @Override
    public RemoteInputStream downloadFile(String fileName) throws RMIIOHTTPException {
        logger.info("downloadFileRMIIO is called");
        RemoteInputStreamServer istream = null;
        GZIPRemoteInputStream gzip = null;
        BufferedInputStream bsi = null;
        FileInputStream fis = null;
        RemoteInputStream result = null;
        try {
            fis = new FileInputStream(fileName);
            bsi = new BufferedInputStream(fis);
            // Create a key of monitoring
            String keyMonitoring = UUID.randomUUID().toString();
            // Instantiate the monitor manager
            RemoteInputStreamMonitorManager rism = new RemoteInputStreamMonitorManager(new File(fileName).length(),
                    keyMonitoring);
            // Wrap the inputstream on a RemoteInputStream
            gzip = new GZIPRemoteInputStream(bsi, rism, 1024) {
                // Override export to get instance of our RemoteStreamExporter
                // implementation
                // Replace the change of system property:
                // System.setProperty(RemoteStreamExporterHttp.EXPORTER_PROPERTY,RISRegistryExporterImpl.class.getName());
                @Override
                public RemoteInputStream export() throws RemoteException {
                    return remoteStreamExporterHTTP.export(this);
                }
            };
            istream = gzip;
            // export the final stream for returning to the client
            // using our exporter implementation
            result = istream.export();
            // Add key of monitor associated to this stream
            ((RemoteInputStreamProxy) result).setKeyMonitoring(keyMonitoring);
            // Set stream to null
            // The client is responsible to close it
            istream = null;

        } catch (FileNotFoundException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        } catch (Throwable e1) {
            logger.info(e1.getMessage());
            throw new RMIIOHTTPException(e1);
        } finally {
            // we will only close the stream here if the server fails before
            // returning an exported stream
            if (istream != null)
                istream.close();
        }
        logger.info("downloadFileRMIIO is done and the result is sended");
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RemoteOutputStream uploadFile(String fileName, double lengthOfFile, String targetFolder)
            throws RMIIOHTTPException {
        logger.info("uploadFileRMIIO is called");
        RemoteOutputStream result;
        RemoteOutputStreamServer ross = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        SimpleRemoteOutputStream sros = null;
        try {
            fos = new FileOutputStream(FileManagerUtils.createFile(targetFolder, fileName));
            bos = new BufferedOutputStream(fos);
            // Generate a key of monitor
            String keyMonitor = UUID.randomUUID().toString();
            // Instantiate the monitor
            RemoteOutputStreamMonitorManager rism = new RemoteOutputStreamMonitorManager(lengthOfFile, keyMonitor);
            // Wrap the outputstream on a RemoteOutputStream
            sros = new SimpleRemoteOutputStream(bos, rism) {
                // Override export to get instance of our RemoteStreamExporter
                // implementation
                // Replace the change of system property:
                // System.setProperty(RemoteStreamExporterHttp.EXPORTER_PROPERTY,RISRegistryExporterImpl.class.getName());
                @Override
                public RemoteOutputStream export() throws RemoteException {
                    return remoteStreamExporterHTTP.export(this);
                }
            };
            ross = sros;
            result = ross.export();
            // Add key of monitoring associated to this stream
            ((RemoteOutputStreamProxy) result).setKeyMonitoring(keyMonitor);
            ross = null;

        } catch (FileNotFoundException e) {
            logger.info(e.getMessage());
            throw new RMIIOHTTPException(e);
        } catch (RemoteException e) {
            logger.info(e.getMessage());
            throw new RMIIOHTTPException(e);
        } catch (Throwable e1) {
            logger.info(e1.getMessage());
            throw new RMIIOHTTPException(e1);
        } finally {
            if (ross != null) {
                ross.close();
            }
        }
        logger.info("uploadFileRMIIO is done and the result is sended");
        return result;
    }

    /**
     * Setter configurationProperties
     * 
     * @param configurationProperties
     */
    public void setConfigurationProperties(Properties configurationProperties) {
        this.configurationProperties = configurationProperties;
    }

    /**
     * Setter remoteStreamExporterHTTP
     * 
     * @param remoteStreamExporterHTTP
     */
    public void setRemoteStreamExporterHTTP(RemoteStreamExporter remoteStreamExporterHTTP) {
        this.remoteStreamExporterHTTP = remoteStreamExporterHTTP;
    }

}
