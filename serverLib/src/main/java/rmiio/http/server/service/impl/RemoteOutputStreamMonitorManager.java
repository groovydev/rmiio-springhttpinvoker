/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import rmiio.http.api.service.MonitoringManagerService;
import rmiio.http.api.service.utils.SpringUtil;

import com.healthmarketscience.rmiio.RemoteOutputStreamMonitor;
import com.healthmarketscience.rmiio.RemoteOutputStreamServer;


/**
 * RemoteOutputStreamMonitorManager
 *  
 */
public class RemoteOutputStreamMonitorManager extends RemoteOutputStreamMonitor {
    /**
     * length of file to transfer
     */
    private final double length;
    /**
     * total bytes transfered
     */
    private double totalBytesTransfered;
    /**
     * Key's monitor used to update percent's transfer
     */
    private String keyMonitor;
    /**
     * Bean monitoring
     */
    private MonitoringManagerService monitoringManagerService;

    /**
     * Constructor
     * 
     * @param length
     * @param keyMonitor
     * @param monitoringManagerService
     */
    public RemoteOutputStreamMonitorManager(double length, String keyMonitor) {
        this.length = length;
        this.keyMonitor = keyMonitor;
        this.monitoringManagerService = (MonitoringManagerService) SpringUtil.getApplicationContext().getBean(
                "monitoringManagerService");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closed(RemoteOutputStreamServer stream, boolean clean) {
        monitoringManagerService.removePercentTransferFile(keyMonitor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void failure(RemoteOutputStreamServer stream, Exception e) {
        monitoringManagerService.removePercentTransferFile(keyMonitor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void localBytesMoved(RemoteOutputStreamServer stream, int numBytes) {
        // Add bytes transfered
        totalBytesTransfered = totalBytesTransfered + numBytes;
        // Calculate percent's transfer
        double percentageTransferdByte = RemoteInputStreamMonitorManager.calculatePercentage(totalBytesTransfered, length);
        // Update percent's transfer
        if (percentageTransferdByte < 100) {
            monitoringManagerService.setPercentTransferFile(keyMonitor, percentageTransferdByte);
        } else {
            monitoringManagerService.setPercentTransferFile(keyMonitor, 100);
        }
    }

}
