/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import rmiio.http.api.service.RegistryOutputStreamsManager;
import rmiio.http.api.service.RemoteOutputStreamProxy;

import com.healthmarketscience.rmiio.RemoteOutputStream;
import com.healthmarketscience.rmiio.RemoteOutputStreamServer;


/**
 * RegistryOutputStreamsManager implementation
 * 
 */
public class RegistryOutputStreamsManagerImpl implements RegistryOutputStreamsManager {

    /**
     * Contains input stream to transfer
     */
    private HashMap<String, RemoteOutputStream> _streams = new HashMap<String, RemoteOutputStream>();

    /**
     * {@inheritDoc}
     */
    @Override
    public RemoteOutputStream register(RemoteOutputStreamServer stream) {
        synchronized (_streams) {
            String key = UUID.randomUUID().toString();
            _streams.put(key, (RemoteOutputStream) stream);
            return new RemoteOutputStreamProxy(key);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unRegister(RemoteOutputStream stream) {
        synchronized (_streams) {
            Set<String> keySet = new HashSet<String>(_streams.keySet());
			for (String key : keySet) {
                if (_streams.get(key).equals(stream)) {
                    _streams.remove(key);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void close(String key, boolean writeSuccess) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                _streams.get(key).close(writeSuccess);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush(String key) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                _streams.get(key).flush();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean usingGZIPCompression(String key) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                return _streams.get(key).usingGZIPCompression();
            }
            return false;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writePacket(String key, byte[] packet, int packetId) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                _streams.get(key).writePacket(packet, packetId);
            }
        }
    }

}
