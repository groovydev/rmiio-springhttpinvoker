/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import rmiio.http.api.service.RegistryInputStreamsManager;
import rmiio.http.api.service.RemoteInputStreamProxy;

import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamServer;


/**
 * RegistryInputStreamsManager implementation</br> We must have one instance exposed and accessible to client;
 * 
 */
public class RegistryInputStreamsManagerImpl implements RegistryInputStreamsManager {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Contains input stream to transfer
     */
    private HashMap<String, RemoteInputStream> _streams = new HashMap<String, RemoteInputStream>();

    /**
     * {@inheritDoc}
     */
    @Override
    public RemoteInputStream register(RemoteInputStreamServer stream) {
        synchronized (_streams) {
            String key = UUID.randomUUID().toString();

            _streams.put(key, (RemoteInputStream) stream);
            return new RemoteInputStreamProxy(key);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unRegister(RemoteInputStream stream) {
        synchronized (_streams) {
            Set<String> keySet = new HashSet<String>(_streams.keySet());
			for (String key : keySet) {
                if (_streams.get(key).equals(stream)) {
                    _streams.remove(key);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] readPacket(String key, int packetId) throws RemoteException, IOException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                return _streams.get(key).readPacket(packetId);
            }
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close(String key, boolean readSuccess) throws RemoteException, IOException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                _streams.get(key).close(readSuccess);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int available(String key) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                return _streams.get(key).available();
            }
            return 0;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long skip(String key, long n, int skipId) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                return _streams.get(key).skip(n, skipId);
            }
            return 0;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean usingGZIPCompression(String key) throws IOException, RemoteException {
        synchronized (_streams) {
            if (_streams.get(key) != null) {
                return _streams.get(key).usingGZIPCompression();
            }
            return false;
        }
    }
}
