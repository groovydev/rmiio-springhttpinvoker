/*******************************************************************************
 * Copyright (c) 2013 Nicolas (https://sourceforge.net/u/cnico).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Nicolas (https://sourceforge.net/u/cnico) - initial API and implementation
 ******************************************************************************/
package rmiio.http.server.service.impl;

import rmiio.http.api.service.MonitoringManagerService;
import rmiio.http.api.service.utils.SpringUtil;

import com.healthmarketscience.rmiio.RemoteInputStreamMonitor;
import com.healthmarketscience.rmiio.RemoteInputStreamServer;


/**
 * RemoteInputStreamMonitorManager
 * 
 */
public class RemoteInputStreamMonitorManager extends RemoteInputStreamMonitor {
    /**
     * Length of file to transfer
     */
    private final double length;
    /**
     * total bytes transfered
     */
    private double totalBytesTransfered;
    /**
     * Key's monitor used to update percent's transfer
     */
    private String keyMonitor;

    /**
     * Bean monitoring
     */
    private MonitoringManagerService monitoringManagerService;

    /**
     * Constructor
     * 
     * @param length
     * @param keyMonitor
     * @param monitoringManagerService
     */
    public RemoteInputStreamMonitorManager(double length, String keyMonitor) {
        this.length = length;
        this.keyMonitor = keyMonitor;
        this.monitoringManagerService = (MonitoringManagerService) SpringUtil.getApplicationContext().getBean(
                "monitoringManagerService");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closed(RemoteInputStreamServer stream, boolean clean) {
        monitoringManagerService.removePercentTransferFile(keyMonitor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void failure(RemoteInputStreamServer stream, Exception e) {
        monitoringManagerService.removePercentTransferFile(keyMonitor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void localBytesMoved(RemoteInputStreamServer stream, int numBytes) {
        // Add bytes transfered
        totalBytesTransfered = totalBytesTransfered + numBytes;
        // Calculate percent's transfer
        double percentageTransferdByte = calculatePercentage(totalBytesTransfered, length);
        // Update percent's transfer
        if (percentageTransferdByte < 100) {
            monitoringManagerService.setPercentTransferFile(keyMonitor, percentageTransferdByte);
        } else {
            monitoringManagerService.setPercentTransferFile(keyMonitor, 100);
        }
    }
    
    public static double calculatePercentage(double part, double totale) {
		double x = (part / totale) * 100;
		long y = (long) (x * 100);
		return (double) y / 100;
	}

}
